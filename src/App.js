import React, {useEffect, useState} from "react";
import './App.css';
import Accordion from "./components/Accordion";

const App = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => setUsers(json));
    }, []);

    return (
        <div>
            {users.length > 0 && users.map((user) => (
                <Accordion user={user}/>
            ))}
        </div>
    );
};
export default App;