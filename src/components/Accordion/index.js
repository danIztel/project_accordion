import React, {useState, useEffect} from 'react';
import "./accordion.css";
const Accordion = ({user}) => {
    const [active, setActive] = useState(false);
    return (
        <>
            <div className="accordion" onClick={()=>setActive(!active)}>{user.name}</div>
            {active && <div className="accordion__modalwindow">
                <h5 className="accordion__modalwindow_text">Email: {user.email}</h5>
                <h5 className="accordion__modalwindow_text">Username: {user.username}</h5>
                <h5 className="accordion__modalwindow_text">Phone: {user.phone}</h5>
                <h5 className="accordion__modalwindow_text">Website: {user.website}</h5>
                <button onClick={() => setActive(false)}
                        className="accordion__modalwindow_button-close">Close</button>
            </div>}








        </>


    );
};
export default Accordion;